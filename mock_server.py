#!/usr/bin/env python3
"""
  * check whether to add 1 bin for random???
"""
from fire import Fire

import ROOT
import time
import datetime as dt
import numpy as np
import re # find livetime and a and b
from console import fg, bg, fx
import os
import random

from gregory_online import histo_np_ops
from gregory_online.histo_global_cont import histograms
from gregory_online.histo_io import load_hist_txt

import datetime as dt





# ==========================================================

def main( hname = "b0c01" , rate = 1000 ):
    """
    with numpy spectrum generator - 10kHz takes 0.2 sec.
    """
    global histograms
    fname = "bg_lt88320.0_a1.05221_b4.211.txt"
    fname = "tot_20230227_112504_milan_bg_pb.txt"
    print()
    mynp,_ = load_hist_txt( fname, "backg" )

    print( histograms["backg"] )
    h = histograms["backg"].Clone("b0c01") # I take the copy of this histo
    histograms["b0c01"] = h
    histo_np_ops.clear( h )
    h.Print()

    # h = ROOT.TH1F(hname,hname,2**15,0,2**15)
    # # histo_np_ops.apply_calib(h, xmin, xmax)

    ene = 1386.5

    s9009 = ROOT.THttpServer("http:9009?loopback")
    s9009.Register("/",h)
    s9009.SetItemField("/", "_monitoring", "1000")
    start = dt.datetime.now()
    mycumsum = np.cumsum(mynp) # PREPARE ONCE CUMUL SUM
    while True:
        time.sleep(1)

        now = dt.datetime.now()
        deltat = now-start
        print(f"i... serving {hname} @{start}  ... {deltat}", end = "\r")
        deltat = int(deltat.total_seconds()*100)/100
        h.SetTitle(f"x rt={deltat}")
        #a = dt.datetime.now()
        for i in range(rate):
            rcont = random.randint( 0,mycumsum[-1] ) # number inside cumsum
            mybin = np.argmax(mycumsum>rcont)
            #print(f"i... generated random bin == {mybin}")
            #h.Fill(mybin) # energy
            h.AddBinContent( int(mybin)+1 ) # ADD ONE OR NOT ??? increment bin (0=underfl)
        #b = dt.datetime.now()
        #print(b-a)

        # ----- creating kind of spe with a peak ---------------
        #for i in range(5):
        #    nor = np.random.normal(loc = 0) *3.2
        #    h.Fill( ene+ nor)
        #for i in range(50):
        #    nor = np.random.uniform(10,50)*np.random.uniform(10,50)
        #    h.Fill( nor)

if __name__=="__main__":
    Fire(main)
