#!/usr/bin/env python3
"""
JUST A BUNCH -- -  GAMES WITH REBIN WITH IMAGE LIBS
"""
from fire import Fire


import numpy as np
import scipy.ndimage

import matplotlib.pyplot as plt
import ROOT
from console import fg, bg, fx





def fill_h_with_np(h1np, hname="total" , runtime = None, errors=None, limits_calib=None):
    """
     histo treatment, histo is in gDirectory, treated by ROOT
    """

    if ROOT.gDirectory.FindObject(hname):
        #print(f"i... {hname} is there")
        histogram = ROOT.gDirectory.FindObject(hname)
    else:
        print(f"{fg.yellow}X... histo={hname} is NOT there, we create now{fg.default}")
        histogram = ROOT.TH1F(hname,hname, len(h1np)-2, 0, len(h1np)-2 )
        print(f"i... {hname} is created now")

    sum = 0
    for i in range( len(h1np) ):
        sum+=h1np[i]
        histogram.SetBinContent(i,h1np[i])
        if not errors is None:
            histogram.SetBinError(i,errors[i])
        # self.h1_diff.SetBinContent(i, self.h1np_diff[i] )
        # #self.h1_dif1.SetBinContent(i, self.h1np_1mdiff[i] )
        # self.h1_dif2.SetBinContent(i, self.h1np_2mdiff[i] )
        histogram.SetEntries(sum)

    if limits_calib is None:
        xmin,xmax = 0, len(h1np)
    else:
        xmin,xmax = limits_calib[0],limits_calib[1]
    #apply_calib(histogram, xmin, xmax)


    #sqrt_done=False
    #if hname != "backg":  # do not repeat it for bg
    #    print("i... doing sumw2==0")
    if errors is None:
        histogram.Sumw2(0) # THIS FOrces sqrt errors
        #sqrt_done = True
        #if sqrt_done:
        #print("i... doing sumw2==1")
        histogram.Sumw2(1) # THIS LET IT TO BE SENSITIVE TO BackG and SCALE



    lowerror_cut = 0.6
    nonzero_mine = 0
    nonzero_min = 0
    if not(h1np is None) and  (len(h1np.nonzero())>1) and (h1np.nonzero()[0].min() >=0):
        if errors is None:
            nonzero_mine = np.sqrt( h1np.nonzero()[0].min() )*lowerror_cut
        else:
            nonzero_mine =  errors.nonzero()[0].min() * lowerror_cut

        # FIND nonezro minimum - error * factor=0.6
        if len(h1np.nonzero())>0:
            nonzero_min = h1np.nonzero()[0].min() - nonzero_mine
        if not runtime is None: # SCALE ALSO ERRORS
            if len(h1np.nonzero())>0:
                nonzero_min = h1np.nonzero()[0].min()/runtime - nonzero_mine/runtime

    # ----------------- scale with runtime
    if not runtime is None:
        histogram.Scale( 1/runtime)
        if len(h1np.nonzero())>0:
            nonzero_min = h1np.nonzero()[0].min()/runtime - nonzero_mine/runtime



    # ------------ this prevents y-axis wobble in rate histogram
    binmx = histogram.GetMaximumBin()
    binco = histogram.GetBinContent( binmx)
    biner = histogram.GetBinError( binmx)

    histomax = 1.01*(binco+biner)
    # histograms[hname].GetYaxis().GetXmin()
    #print(f"D... nonzero_min =   {nonzero_min}")
    histogram.GetYaxis().SetRangeUser( nonzero_min, histomax )
    return histogram








def rebin_np( spectrum , sca, shi, order = 3):
    """
    should do sca* spectrum + shi .... order 3 has sometimes lower errors
    """
    spectrumb = scipy.ndimage.zoom(spectrum, sca, order=order)
    spectrumb = scipy.ndimage.interpolation.shift(spectrumb, shi , cval=0, order = order)
    asu = spectrum.sum()
    bsu = spectrumb.sum()

    spectrumb=spectrumb/bsu*asu

    df = len(spectrumb) - len(spectrum)
    #print( f" b={len(b)}    a={len(a)}   df = {df}")
    while df<0:
        spectrumb = np.append( spectrumb,[0] )
        df = len(spectrumb) - len(spectrum)
    if df>0:
        spectrumb = spectrumb[:-df]
    #print( len(a),"X" ,len(b),  f" 6 -->  {6*sca+shi}" )
    #print( len(a),"X" ,len(b),  f" 22 -->  {22*sca+shi}" )
    kan = 3828
    print( f"i...  channel {kan} --->  {sca* kan + shi:.1f} ")
    return spectrumb



def load_np( filename):
    with open(filename,"r") as f:
        spe = f.readlines()
    spe = [ float(x.strip()) for x in spe ]
    #spe = spe[:100]
    #print(spe, len(spe) )
    #return spe
    npspe = np.array(spe)
    print(npspe)
    return npspe

def save_np(  spectrum , name ):
    fname = name+".asc1"
    with open(fname,"w") as f:
        for i in spectrum:
            j=0
            if i>0: j=i
            f.write( f"{j:.2f}\n" )
    return None



#***********************************************************************
from pyfromroot import prun
import time
def main():
    """
    I want to rebin the spectrum and do tests.......
    USING pyfromroot
    """

    filename = "run_122_04.txt"
    print()

    for ii in range(150):
        for jj in range(150):
            A = 0.5 + float(ii)*0.02
            B = -0.2 + float(jj)*0.002

            nn = load_np( filename )
            n2 = rebin_np( nn, A,B )  # I NEED TO DO ERRORS

            save_np(nn, "nn")
            save_np(n2, "n2")  # I NEED TO SAVE ERRORS

            prun.do("load","n2.asc1 h")  # I NEED TO LOAD HISTO WITH ERRORS
            prun.do("zoom",f"n2 {3986*A+B},{30*A}") # 14042.88   154
            prun.do("draw","n2", canvas = "cn", opt="logy", color="red" )
            res2 = prun.do("fit","n2 gpol1", canvas="fitted n2")  #

            print(res2.keys() )
            area = res2['area']
            darea = res2['darea']

            with open("stability.dat","a") as f:
                f.write( f"{A:.3f} {B:.4f} {area:.2f} {darea:.2f}\n" )

    while ROOT.addressof(ROOT.gPad)!=0: time.sleep(0.2)
    return


    hnn = fill_h_with_np(nn,"nn" )
    hn2 = fill_h_with_np(n2,"n2" )

    # histograms
    hnn.Draw()
    hn2.Draw("same")
    hn2.SetLineColor(2)


    #    plt.plot(  nn , '.-')
    #    plt.plot(  n2 , '.-r')
    #    plt.show()
    input()


#*****************************************************************
if __name__=="__main__":
    Fire(main)
