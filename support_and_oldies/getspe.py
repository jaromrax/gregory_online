#!/usr/bin/env python3


import urllib.request
import json


import matplotlib.pyplot as plt
from fire import Fire

import ROOT

import time
import datetime as dt

import numpy as np

# peak find .... TSpectrum is legacy code now, 2022
#
from scipy.signal import find_peaks_cwt

from pyfromroot import  prun

import os

from console import fg,bg,fx

import socket

import threading

#--------https://stackoverflow.com/questions/2408560/python-nonblocking-console-input
# ON ENTER
# CALL WITH CALLBACK FUNCTION AS A PARAMETER
class KeyboardThread(threading.Thread):

    def __init__(self, input_cbk = None, name='keyboard-input-thread'):
        self.input_cbk = input_cbk
        self.block = False
        super(KeyboardThread, self).__init__(name=name)
        self.start()

    def run(self):
        while True:
            self.input_cbk(input()) #waits to get input + Return

    def pause(self):
        self.block = True

    def unpause(self):
        self.block = False


KEYINPUT=None

def my_callback(inp):
    """
    Read input + enter from keyboard. In the thread.
    """
    global KEYINPUT
    #evaluate the keyboard input

    print('You Entered: /{}/'.format(inp) , flush = True)
    #time.sleep(2)

    KEYINPUT = inp
    return

#start the Keyboard thread
kthread = KeyboardThread(my_callback)






def udpsend(message):

    UDP_IP = "127.0.0.1"
    UDP_PORT = 8100
    MESSAGE = message
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
    sock.sendto(bytes(MESSAGE, "utf-8"), (UDP_IP, UDP_PORT))
    print(f"D... {fg.green}UDPsent {MESSAGE}{fg.default}")


def zoom_peak( hname , analyze_e , started , m1_zero, m1_no  ):
    """
    Just zoom to see if gPad is ok to work with
    """
    if started is None:
        started = dt.datetime.now()
    #hname = "h1"
    #
    #nalyze_e = 1460
    #for i in self.indices_cal:
    #    e = float(i)
    #    if abs(e-analyze_e)<10:
    e = float(analyze_e)
    locastr = f"{e-15},{e+15}" # at interval +-15 channels(kev?)


    if 'locastr' in locals():
        ###prun.loadpy("load",f"{fname} h,cala=0.6763,calb=1.0783")
        prun.loadpy("zoom",f"{hname} {locastr}")




def analyze_peak_simple( hname, analyze_e , e_window = 15 ):

    e = float(analyze_e)
    de = e_window
    locastr = f"{e-de},{e+de}" # at interval +-15 channels(kev?)

    if 'locastr' in locals():
        ###prun.loadpy("load",f"{fname} h,cala=0.6763,calb=1.0783")
        prun.loadpy("zoom",f"{hname} {locastr}")

        #if self.h1_dif1.GetMaximum()>4: # at least 4 pulses

        res = prun.loadpy("fit",f"{hname} gpol1")  #print(res.keys() )

        if abs(res['diff_fit_int_proc'])>1:
            print("X...  BAD DESCRIPTION - DIFF MORE THAN 1%")
            time.sleep(0.2)
            return None

        elif not( res['noerror']):
            print("X...  BAD DESCRIPTION - SOME ERROR OF FIT")
            time.sleep(0.2)
            return None

        elif res['area'] <= 2*res['darea']:
            print("X...  BAD DESCRIPTION - Area error too large")
            time.sleep(0.2)
            return None

        elif res['area'] <= 7:
            print("X...  BAD DESCRIPTION - SMALL Area < 7")
            time.sleep(0.2)
            return None
        else:
            return res # YES, RESULT
    else:
        return None # not no present locastr.....







def analyze_peak( hname , analyze_e , started , m1_zero, m1_no , e_window = 15 ):
    """
    Here I use prun to fit the histogram region with gaus+pol1
    """

    if started is None:
        started = dt.datetime.now()
    #hname = "h1"
    #
    #nalyze_e = 1460
    #for i in self.indices_cal:
    #    e = float(i)
    #    if abs(e-analyze_e)<10:
    e = float(analyze_e)
    de = e_window
    locastr = f"{e-de},{e+de}" # at interval +-15 channels(kev?)


    if 'locastr' in locals():
        ###prun.loadpy("load",f"{fname} h,cala=0.6763,calb=1.0783")
        prun.loadpy("zoom",f"{hname} {locastr}")

        #if self.h1_dif1.GetMaximum()>4: # at least 4 pulses

        res = prun.loadpy("fit",f"{hname} gpol1")  #print(res.keys() )

        if abs(res['diff_fit_int_proc'])>1:
            print("X...  BAD DESCRIPTION - DIFF MORE THAN 1%")
            time.sleep(0.2)

        elif not( res['noerror']):
            print("X...  BAD DESCRIPTION - SOME ERROR OF FIT")
            time.sleep(0.2)
            return

        elif res['area'] <= 2*res['darea']:
            print("X...  BAD DESCRIPTION - Area error too large")
            time.sleep(0.2)

        elif res['area'] <= 7:
            print("X...  BAD DESCRIPTION - SMALL Area < 7")
            time.sleep(0.2)

        else:
            now = dt.datetime.now()
            wri = f"{(now-started).total_seconds():8.1f} {m1_no:8.0f} {m1_zero:8.0f} {res['E']:9.2f} {res['dE']:8.3f} {res['area']:12.2f} {res['darea']:10.2f} {res['Efwhm']:10.3f} {res['dEfwhm']:7.2f}\n"
            print(wri)


            tag = f"{started}"
            outname = f"zprog_{tag.replace(' ','_')}_{analyze_e}.txt"
            writecol = True
            if os.path.isfile( outname ): writecol = False
            with open(outname,"a") as f:
                 if writecol: f.write("#COLNAME: time,noise,zero,e,de,area,darea,efwhm,defwhm\n")
                 f.write(wri)

            # area1385 : area
            res2 = f'"area{analyze_e:.0f}":{res["area"]:.2f},"area{analyze_e:.0f}up":{res["area"]+res["darea"]:.2f},"area{analyze_e:.0f}down":{res["area"]-res["darea"]:.2f}'

            res2 = 'influxme [{"measurement":"gregory_peaks","fields":{'+res2+'}}]'


            # return f'"area{analyze_e}"={res["area"]},"darea{analyze_e}"={res["darea"]}'
            udpsend(res2 )








def detect_peaks(h1np, a, b):
    # h1np is numpy field ....   h1 is TH1F
    # a,b calibration

    pkmin = 0
    pkmax = 5000
    N_EMIN = 20 # 20 kev noise min

    #-------------------------------------------------------------
    # PLAN 1-  2.8 noise_perc, snr 3 2/a-5/a TO GET RANGE
    # PLAN 2-  11 noise_perc, snr 3 2/a-5/a  TO GET DETAILS

    #print(" ... LINE 238 ", len(h1np) )
    #print( h1np )
    #print( h1np[0] )
    #print(  h1np[0:4000] )

    # indices = find_peaks_cwt( h1np[0:4000] ,
    #                           np.arange(2/a, 5/2),  # 2,6 looked good
    #                           #np.arange(width,width+1),
    #                           noise_perc=  3,
    #                           min_snr = 3 )

    # # only for more >10 pulses in peak
    # indices = [ i for i in indices if h1np[ int(i) ]>5]
    # strong_peaks = len(indices)
    # # Energies; only with E> N_EMIN (20) keV
    # indices_cal = [ round(10*(i*a+b))/10 for i in indices if round(10*(i*a+b))/10 >N_EMIN]
    # #print("i... INDICES CAL", indices_cal)
    # print("i... E_PEAKS", indices_cal)

    # return indices_cal
    return []

    #PLAN2 HERE----------------------------
    indices = find_peaks_cwt( h1np[0:4000] ,
                              np.arange(1.5/a, 4.5/2),
                              noise_perc=  11, # cut sensitivity at 11%
                              min_snr = 2.5 )  # set SNR to 4 (2.5 is the edge - FDR^)
    # only for more >10 pulses in peak
    indices = [ i for i in indices if h1np[ int(i) ]>10]
    indices_cal = [ round(10*(i*a+b))/10 for i in indices if round(10*(i*a+b))/10 >N_EMIN]
    # restrict range
    indices_cal = [ i for i in indices_cal if (i>pkmin-10)and(i<pkmax+10)]


    if False:
        #---------------- now play with graphs --------------
        # print(self.h1np)
        # range 7-20-------------
        #  snr 6 - peaks 7
        #      3 - 13
        #      2 - 19
        # -----------10- 20 missing low energy
        # ----------4-10  nsr2   26 peaks -- this is quite good

        # noise 3 better with width 1-5

        # for noise - (1 is no good)
        width0 = 0.5
        width = width0
        orde =0
        for gg in self.graphs:
            orde+=1
            width+=1
            indices = find_peaks_cwt( self.h1np[0:4000] ,
                                      np.arange(1.5/a, 4/a),  # 2,6 looked good
                                      noise_perc=  11,
                                      min_snr = width )  # noise 30 2remain

            self.indices_cal = [ round(10*(i*a+b))/10 for i in indices if round(10*(i*a+b))/10 >0]
            self.indices_cal = [ i for i in self.indices_cal if (i>pkmin-10)and(i<pkmax+10)]

            gg.Set(0)
            for i in self.indices_cal:
                gg.SetPoint( gg.GetN(), int(i) , (1 + orde*0.3)*self.h1.GetBinContent( int( (i-b)/a ))  )

            print(f"i... peak find...  {len(self.indices_cal)}  width(or SNR) = {width} ")
            #print(f"{indices}")
            #print(f"{self.indices_cal}")
            gg.Draw("PL")
    #-------------------------END OF PLAYING WITH DETECTION ----------------
    return indices_cal











class Histogram:

    #------------------------------------------------------------

    def __init__(self, url, spectrum="b0c00", peaksfile=None, calibrationfile=None, bgfile=None ):
        """
        init & first draw; calls self.wait at the end == runs in loop
        """

        self.started = dt.datetime.now()

        self.measurement = "hpge" # string in INFLUXDB

        self.url = f'http://{url}:9009/{spectrum}/root.json'
        self.spectrumname = spectrum
        print(f"i... url = {self.url}")

        self.size = 2**15 # histogram size

        self.maximum = 10 # default of histomax, but overriden immediat

        self.N_EMIN = 20 # 20keV noise end by definition

        self.e_window = 15 # +- Window to ZOOM and Analyze


        self.first_influx_omit = True # ONETIME TRIG

        self.bgfile = bgfile # others are used later....  BACKGROUND FILE
        self.bgfile = "bg88320.txt" # others are used later....  BACKGROUND FILE
        self.bgtime = 88320
        #-------------------- timing of display

        self.SECONDS = 10
        self.MINUTE = 60 # 5minutes (300)
        #self.MINUTE = 60 #

        self.HOUR = 3600
        #self.HOUR = 60




        print(f"""
i... {fg.red}RED         HISTO  every {self.SECONDS} s{fg.default}
i... {fg.cyan}LIGHT BLUE  HISTO  every {self.MINUTE} s{fg.default}
i... {fg.magenta}MAGENTA     HISTO  every {self.HOUR} s{fg.default}
i... {fg.blue}DARK BLUE   HISTO  TOTAL{fg.default}
""")




        # calibration that comes from GetXaxis()
        self.xmin = 0
        self.xmax = self.size

        #------------------------------------------------ init NP and TH
        self.g_noise = ROOT.TGraph()
        self.g_zeros = ROOT.TGraph()
        self.g_noise.SetMarkerStyle(21)
        self.g_zeros.SetMarkerStyle(24)
        self.g_zeros.SetMarkerColor(3)

        self.g_mgr=  ROOT.TMultiGraph()
        self.g_mgr.Add(self.g_noise,"P")
        self.g_mgr.Add(self.g_zeros,"L")
        # self.g_mgr.Draw("ap")

        #------------------------------------------------ init NP and TH
        self.h1np = np.zeros( self.size+2)
        self.h1np_prev = np.zeros( self.size+2)
        self.h1np_1m = np.zeros( self.size+2)
        self.h1np_1mdiff = np.zeros( self.size+2)
        self.h1np_2m = np.zeros( self.size+2)
        self.h1np_2mdiff = np.zeros( self.size+2)

        self.h1      = ROOT.TH1F("h1","total histogram",   self.size, 0, self.size )
        self.h1_diff = ROOT.TH1F("h1diff",f"{self.SECONDS} seconds", self.size, 0, self.size )
        self.h1_dif1 = ROOT.TH1F("h1dif1",f"{self.MINUTE} seconds", self.size, 0, self.size )
        self.h1_dif2 = ROOT.TH1F("h1dif2",f"{self.HOUR} seconds   ", self.size, 0, self.size )


        # ----graph
        self.graphs=[]
        for i in range(0,10):
            if i==4:
                continue
            g1 = ROOT.TGraph()
            g1.SetPoint( g1.GetN(), 100,100)
            g1.SetMarkerStyle(23+i) # 21 rect; 22 triup; 23 OK
            g1.SetMarkerColor(1+i) #
            g1.SetLineColor(1+i) #
            self.graphs.append( g1 )





        # #---------------------------------------------- CANVASGAMES
        # self.cannameg = "c_graph"
        # self.c1g = ROOT.TCanvas( self.cannameg ,"c_graph")
        # self.c1g.Draw()

        # MAIN
        # self.canname = "c1"
        # self.c1 = ROOT.TCanvas( self.canname ,"c1")
        # self.c1.Draw()

        self.canname0 = "c0"
        self.c0 = ROOT.TCanvas( self.canname0 ,f"c0_{spectrum}",900,400)
        self.c0.Draw()
        self.c0.cd()

        self.c0.Divide(2,1)
        self.c0.cd(1)

        #self.canname = "c1"
        #self.c1 = ROOT.addressof(ROOT.gPad)
        #self.c1.Draw()
        #self.c1.cd()
        self.h1.Print()
        self.h1.GetXaxis().SetRangeUser(0,4000)


        self.reload_cfg_files( peaksfile, calibrationfile )

        #-------------------------- ha problem, 1st draw before other definitions -----------
        self.draw()
        ROOT.gPad.SetLogy()
        ROOT.gPad.SetGridx()
        ROOT.gPad.SetGridy()


        # ------------------------------ canvas to zoom a peak
        # self.cannamezoom = "czoom"
        # self.czoom = ROOT.TCanvas( self.cannamezoom ,"czoom")
        # self.czoom.Draw()
        self.c0.cd(2) # GO TO 2nd PANEL
        self.h1_dif1.Draw("HE0X0")
        #self.czoom = self.c0.GetPad(2) #ROOT.addressof(ROOT.gPad, byref=True) #
        #self.czoom.Draw()
        # ROOT.gPad.SetLogy() NOT LOGY
        ROOT.gPad.SetGridx()
        ROOT.gPad.SetGridy()

        #self.c1().cd()
        #ROOT.gPad.SetLogy()
        #ROOT.gPad.SetGridx()
        #ROOT.gPad.SetGridy()



        self.wait()



#------------------------------------------ END OF INIT -------------------------


    def reload_cfg_files(self, peaksfile, calibrationfile):
        #---------------------------------- loading peak file
        #                                  will go through and fit and log
        self.test_peaks = []
        if not (peaksfile is None):
            with open(peaksfile) as f:
                pks = f.readlines()
            #print(pks)
            #pks = pks.split("\n")
            pks = [x.strip() for x in pks]
            print("PEAKS TO TEST ... LOADED:", pks )
            self.test_peaks = pks

        #---------------------------------- loading calibration energy file
        #                                  will go through and fit and log
        #print("... reloading CALIBRAION")
        self.test_calibration = []
        if not (calibrationfile is None) and not (calibrationfile==""):
            with open(calibrationfile) as f:
                cals = f.readlines()

            #print(cals)
            # cals = pks.split("\n")

            cals = [ x.strip() for x in cals]
            cals = [ x.replace("    "," ").replace("   "," ").replace("  "," ").strip() for x in cals]
            print(cals)
            cals = [ x.split(" ") for x in cals if x.find("#")<0]
            print("CALIBRATION TO TEST ... LOADED:", cals )
            self.test_calibration = cals








    def czoom( self ):
        # print("czoom",self.c0)
        # print("czoom",self.c0.GetPad(0))
        # print("czoom",self.c0.GetPad(1))
        # print("czoom",self.c0.GetPad(2))
        return self.c0.GetPad(2)

    def c1( self ):
        return self.c0.GetPad(1)




    def export2influx_persec(self, jsonpart=""):
        if jsonpart!="":
            print(jsonpart)
            return
        # avoid 1st spectrum---------------difference huge
        if self.first_influx_omit:
            self.first_influx_omit = False
            return

        board   = self.spectrumname[:2]
        channel = int(self.spectrumname[-2:])

        pup = self.h1np_diff[1]/self.SECONDS # chnnel 1 is ZERO CHANNEL
        rate = self.h1np_diff.sum()/self.SECONDS
        reg = self.h1np_diff[:10].sum()/self.SECONDS # region
        udp_seread =f"gregory_{self.measurement}: {board} ch{channel} {rate:.3f} {pup:.3f} {reg:.3f}"

        # rate, pup reg: region is  chanels 0-10
        udpsend( udp_seread )


#         print(f"""
# chan1  == {pup} (pup)
# chan2  == {self.h1np_diff[2]/self.SECONDS}
# chanL  == {self.h1np_diff[-1]/self.SECONDS}
#  sum   == {rate}
# sumF10 == {reg}
# sumL10 == {self.h1np_diff[-10:].sum()/self.SECONDS}
#  len == {len(self.h1np_diff)}
# """)
        return


    #------------------------------------------------------------
    def get_cal(self):
        a = (self.xmax-self.xmin)/self.size
        b = self.xmax - a*self.h1.GetXaxis().GetNbins()
        return a,b




    #------------------------------------------------------------
    def sum_noise(self, histo, name = ""):

        czero = histo.GetBinContent(1) # bin 0 is underflow
        sumn  = histo.GetBinContent(2) # bin 0 is underflow, 1 is zero, 2 is just noise

        # a,b = self.get_cal()

        # f=histo.GetFunction("expo")
        # if ROOT.addressof(f)!=0:
        #     f.Delete()

        # histo.Fit("expo","Q","",b, self.N_EMIN)
        # f=histo.GetFunction("expo")
        # if ROOT.addressof(f)==0:
        #     return 1,czero

        # sumn =f.Integral(b,self.N_EMIN)

        # print(f"... NOISE {name:7s}: sum = {sumn:9.0f}  / chan0 = {czero:9.0f}  cnts")
        return sumn, czero













    #------------------------------------------------------------
    def draw(self):
        global KEYINPUT
        #print("i... draw h1", self.h1)
        #self.h1.Print()

        #if ROOT.addressof(self.g_mgr)!=0:
            # #--------multigraph ------------------- GRAPH
            # self.c1g.cd()
            # #self.g_mgr.Print()
            # if self.g_zeros.GetN()>0:
            #     self.g_mgr.Draw("pawl")
            #     self.g_mgr.GetXaxis().SetTimeDisplay(1)
            #     self.g_mgr.GetXaxis().SetNdivisions(503)
            #     self.g_mgr.GetXaxis().SetTimeFormat("%Y-%m-%d %H:%M")
            #     #ROOT.gSystem->Setenv("TZ","UTC");
            #     self.g_mgr.GetXaxis().SetTimeOffset(0,"cest")
            #     ROOT.gPad.SetGridx()
            #     ROOT.gPad.SetGridy()
            #     self.c1g.Modified()
            #     self.c1g.Update()

        if self.h1 is None:
            return
        if self.h1np is None:
            return


        self.c1().cd() #

        self.h1.Draw("HEX0") # E0... bars, X0 no 0  H-histoLines   was HE0X0, now HEX0

        a,b = self.get_cal()


        # # peak detections
        #
        #------------------------------------------------ THIS IS EVERY 3seconds DETECTION
        #
        #print(" ...LINE 638", len(self.h1np ) )
        indices_cal = detect_peaks( self.h1np, a=a, b=b ) # TOTAL OK
        #






        # if len(indices_cal)>1:

        #     pkmin = indices_cal[0]
        #     pkmax= indices_cal[-1]
        #     #print(f" ... peak min - max {pkmin} - {pkmax}")

        #     self.graphs[1].Set(0)
        #     orde=0
        #     for i in indices_cal:
        #         orde+=1

        #         # print(f"{orde:3d}: {i:7.1f} keV")

        #         self.graphs[1].SetPoint( self.graphs[1].GetN(), int(i) , 1.2*self.h1.GetBinContent( int( (i-b)/a ))  )
        #     self.graphs[1].SetMarkerStyle(29)
        #     self.graphs[1].SetMarkerSize(1.4)
        #     self.graphs[1].Draw("P")

        # if len(indices_cal)>1:
        #     self.graphs[2].Set(0)
        #     for i in self.indices_cal:
        #         self.graphs[2].SetPoint( self.graphs[2].GetN(), int(i) , self.h1.GetBinContent( int( (i-b)/a ))  )
        #     self.graphs[2].Draw("PL")
        #     self.graphs[2].SetLineStyle(3)
        #     print(f"... {strong_peaks} peaks; at lower SNR: {len(self.indices_cal)} peaks detected at ({pkmin}-{pkmax}) keV")




        #self.h1.GetXaxis().SetRangeUser(1,4000) # zoom

        hmax = self.h1.GetBinContent(self.h1.GetMaximumBin())

        if hmax ==0:
            hmax = self.maximum
        #print("--------------------------hmax",hmax)
        # -------- actual spectrum -0--------------
        self.h1.GetYaxis().SetRangeUser(0.1, hmax)


        # Dont draw HOUR
        ## color 3 green;  color 5 yellow  # 2 is HOUR
        #self.h1_dif2.Draw("same HEX0")
        #self.h1_dif2.SetLineColor(6)
        ##self.h1_dif2.GetXaxis().SetRangeUser(1,4000)

        self.h1_dif1.Draw("same HEX0")  # 1 is minute
        self.h1_dif1.SetLineColor(4)
        #self.h1_dif2.GetXaxis().SetRangeUser(1,4000)

        self.h1_diff.Draw("same HEX0")
        self.h1_diff.SetLineColor(2) # red
        #self.h1_diff.GetXaxis().SetRangeUser(1,4000)





        #-------------------------------------------------------------- COMMANDS
        # t
        # sbg
        # f 1460
        # c


        if not (KEYINPUT is None) and (KEYINPUT.find("t")==0):
            if len(KEYINPUT.split(" "))>1:
                TIME = KEYINPUT.split(" ")[-1].strip()
                h=0
                if TIME.find(":")<0:
                    try:
                        m = 0
                        s=float(TIME)
                    except:
                        m,s = 0,1
                if len(TIME.split(":"))==2:
                    try:
                        m,s=TIME.split(":")
                    except:
                        m,s = 0,1
                if len(TIME.split(":"))==3:
                    try:
                        h,m,s=TIME.split(":")
                    except:
                        h,m,s= 0,0,1

                runtime = float( h)*3600+float(m)*60+float(s)
                self.started=dt.datetime.now() - dt.timedelta(seconds=runtime)
                print("i... reset start time")
                KEYINPUT = None
            else:
                runtime = (dt.datetime.now()-self.started).total_seconds()
            KEYINPUT = None



        if not (KEYINPUT is None) and (KEYINPUT.find("sbg")==0):
            runtime = (dt.datetime.now()-self.started).total_seconds()

            print(f"****************************** sub BG on demand, runtime={runtime}")
            self.h1subbg = self.h1.Clone("h1subbg")
            #self.h1subbg.Sumw2(0) # should inherit it from h1...
            # open file
            with open(self.bgfile) as f:
                speb = f.readlines()
            speb = [x.strip() for x in speb]

            #sum = 0
            for i in range( len(speb) ):
                # _mdiff arrays are filled / operated inside W A I T
                #sum+=int(speb[i])/self.bgtime*runtime
                self.h1subbg.SetBinContent(i, int(speb[i])/self.bgtime*runtime)

            self.czoom().cd()
            self.h1subbg.Add( self.h1, self.h1subbg, 1, -1 )
            #for i in range( len(speb) ):
            #    self.h1subbg.SetBinError(i, self.h1subbg.GetBinError(i)*3 )
            self.h1subbg.Draw("E1")





        if not (KEYINPUT is None) and (KEYINPUT.find("f ")==0):
            print("****************************** fit on demand")
            peake = float(KEYINPUT.replace("   "," ").replace("  "," ").split(" ")[-1])
            print("C... FIT {peake} keV")
            KEYINPUT = None
            self.czoom().cd()
            ROOT.gPad.SetLogy(0)
            self.h1manu = self.h1.Clone("h1manu")
            self.h1manu.Draw()
            res = analyze_peak_simple(  "h1manu",  float(peake),  e_window=self.e_window )

        #---------------------------- automatic calibration

        if not (KEYINPUT is None) and (KEYINPUT=="c"):
            KEYINPUT = None
            self.czoom().cd()                 #print(ROOT.gDirectory.ls())
            regr_fieldx = []
            regr_fieldy = []
            self.h1calib = self.h1.Clone("h1calib")
            # uncalibrate
            self.h1calib.GetXaxis().SetLimits(0,self.h1calib.GetNbinsX())
            self.h1calib.Draw()

            print("LINE 797")
            print(self.test_calibration)

            for (ene,chan) in self.test_calibration:
                #def analyze_peak( hname , analyze_e , started , m1_zero, m1_no , e_window = 15 , return_ene_only=False):
                res = analyze_peak_simple(  "h1calib",  float(chan),  e_window=self.e_window )
                if not(res is None):
                    print(res['E'], ene)
                    regr_fieldx.append(res['E'])
                    regr_fieldy.append( float(ene) )
            if len(regr_fieldx)>=2:
                resfit = np.polyfit(regr_fieldx,regr_fieldy,1)
                print(regr_fieldx)
                print(regr_fieldy)
                print(f"i... calculated calibration {resfit[0]:.5f} {resfit[1]:.4f}")

            self.czoom().Modified()
            self.czoom().Update()
            self.c0.Modified()
            self.c0.Update()
            self.c1().cd() # go back to main panel





    #------------------------------------------------------------

    def fetch_histo(self):
        """
        Every fetch, numpy arrays are filled /  TH1F content is set too
        """
        ok = False
        try:
            with urllib.request.urlopen(self.url) as response:
                jhis = response.read().decode("utf8")
            ok = True
        except:
            ok = False

        if not ok : return False



        jhis = json.loads(jhis)
        # for i in jhis.keys(): if i!="fArray": print( f"{i:20s} {jhis[i]} " )

        self.xmin = jhis['fXaxis']['fXmin']
        self.xmax = jhis['fXaxis']['fXmax']

        if (self.xmin!=self.h1.GetXaxis().GetXmin() ) or (self.xmax!=self.h1.GetXaxis().GetXmax() ):
            print(self.xmin,self.xmax,self.h1.GetXaxis().GetXmin(),self.h1.GetXaxis().GetXmax())
            # calibrate ALL
            self.h1.GetXaxis().SetLimits(self.xmin,self.xmax)
            self.h1_diff.GetXaxis().SetLimits(self.xmin,self.xmax)
            self.h1_dif1.GetXaxis().SetLimits(self.xmin,self.xmax)
            self.h1_dif2.GetXaxis().SetLimits(self.xmin,self.xmax)

            print(self.xmin,self.xmax,self.h1.GetXaxis().GetXmin(),self.h1.GetXaxis().GetXmax())


        helem = jhis['fArray']

        self.h1np_prev = self.h1np # save  previous
        self.h1np = np.array( helem )
        #if self.h1np_prev is None:
        #    self.h1np_prev = self.h1np

        #if self.h1np_prev is None:
        #    self.h1np_diff = self.h1np
        #else:

        #---------protection to reset
        if self.h1np.sum()< self.h1np_prev.sum():
            self.h1np_prev = self.h1np_prev*0
            self.h1np_1m = self.h1np
            self.h1np_2m = self.h1np

        self.h1np_diff = self.h1np   - self.h1np_prev



        if self.h1np_1m is None:
            self.h1np_1m = self.h1np
        if self.h1np_2m is None:
            self.h1np_2m = self.h1np


        sum = 0
        #print("H1----------------")
        #self.h1.Print()


        #-----UUUUUUuuu   diff are not filled yet here

        self.maximum = max(helem)

        for i in range( len(helem) ):
            # _mdiff arrays are filled / operated inside W A I T
            sum+=helem[i]
            self.h1.SetBinContent(i,self.h1np[i])
            self.h1_diff.SetBinContent(i, self.h1np_diff[i] )
            #self.h1_dif1.SetBinContent(i, self.h1np_1mdiff[i] )
            self.h1_dif2.SetBinContent(i, self.h1np_2mdiff[i] )
            self.h1.SetEntries(sum)

        self.h1.Sumw2(0) # This resutls in sqrt error
        self.h1_diff.Sumw2(0) # This resutls in sqrt error
        self.h1_dif2.Sumw2(0) # This resutls in sqrt error
        # AND THEN CREATE GOOD ERROR TREEATMENT
        self.h1.Sumw2(1) # This resutls in sqrt error
        self.h1_diff.Sumw2(1) # This resutls in sqrt error
        self.h1_dif2.Sumw2(1) # This resutls in sqrt error


        return True





    #------------------------------------------------------------
    #  MAIN LOOP HERE
    #------------------------------------------------------------
    def wait(self):
        """
        fetches histo ; self.draw;  plays with numpys per minute...
        """
        self.c1().Modified()
        self.c1().Update()

        last = dt.datetime.now()
        last1m = last
        last2m = last

        c1addr = ROOT.addressof(self.c1())
        i = 0
        while True:
            i+=1
            time.sleep(0.3)

            now = dt.datetime.now()
            tot = self.SECONDS - (now - last).total_seconds()

            # --------------------rotatig marker
            now = dt.datetime.now()
            runtime = (now-self.started).total_seconds()
            h = int(runtime/3600)
            m = int((runtime - 3600*h)/60)
            s = int(runtime - 3600*h -60*m)
            runtime = f"{h:02d}:{m:02d}:{s:02d} "
            if (i%4)==0:
                print(f" ... {runtime} waiting / {tot:4.0f}" ,end="\r" )
            elif (i%4)==1:
                print(f" ... {runtime} waiting - {tot:4.0f}" ,end="\r" )
            elif (i%4)==2:
                print(f" ... {runtime} waiting \ {tot:4.0f}" ,end="\r" )
            else:
                print(f" ... {runtime} waiting | {tot:4.0f}",end="\r" )


            # print( "\n", ROOT.gDirectory.ls() ) # h1

            if ROOT.addressof(ROOT.gPad)==0: # GPAD
                print("gpad none     == canvas closed ....  ")
                break


            #
            # FAST RED LOOP
            #
            if ( now - last).total_seconds()>self.SECONDS:
                last = now
                #print(f"i... last {last}")

                now1 = dt.datetime.now()
                resok = self.fetch_histo(  )
                if not resok:
                    print("X... no response ...........")
                    continue
                now2 = dt.datetime.now()
                # print(f"i... HISTO fetch lasts ~ {(now2-now1).total_seconds():.3f} sec.")

                self.draw() # detects peaks too............ bad idea here

                #
                # INFLUXDB save rates HERE   self.h1_diff  self.h1np_diff
                #
                self.export2influx_persec()

                self.c1().Modified()
                self.c1().Update()



            #
            # when 1 minute (5min) read markers
            #
            if ( now - last1m).total_seconds()>self.MINUTE:


                if sum(self.h1np_1m)>0: # I protect first minute
                    # protection for reset
                    if self.h1np.sum() < self.h1np_1m.sum():
                        self.h1np_1m = self.h1np_1m*0

                    self.h1np_1mdiff = self.h1np - self.h1np_1m
                    helem = self.h1np_1mdiff
                    suma = 0
                    for i in range( len(helem) ): # FILL HIST
                        suma+=helem[i]
                        self.h1_dif1.SetBinContent(i, self.h1np_1mdiff[i] )
                    self.h1_dif1.SetEntries(suma)
                    self.h1_dif1.Sumw2(0)
                    # AND aplly good error treatment
                    self.h1_dif1.Sumw2(1)

                self.h1np_1m = self.h1np
                last1m = now
                #print("                                ****** last 1m", end = "\n")


                # GET NOISE VALUES m1_no == 2nd channel ; m1_zero == 1stchannel
                tot_no,tot_zero = self.sum_noise( self.h1, "total")
                m1_no, m1_zero =  self.sum_noise( self.h1_dif1, "minute") # 1 minute

                roonow = ROOT.TDatime( now.year, now.month, now.day,
                                       now.hour, now.minute,now.second )
                #roonow.Print()
                # 2011,2,28,15,53,0)
                self.g_noise.SetPoint( self.g_noise.GetN() , roonow.Convert(), m1_no )
                self.g_zeros.SetPoint( self.g_zeros.GetN() , roonow.Convert(), m1_zero )
                # self.g_mgr= ROOT.TMultiGraph()
                # self.g_mgr.Add(self.g_noise,"*")
                # self.g_mgr.Add(self.g_zeros,"C*")
                #self.g_mgr.Print()




                #--------------------------- analyze h1diff1---------------
                a,b = self.get_cal()
                print(f"D... {fg.cyan}detecting peaks in {self.MINUTE} sec. difference: {self.h1np_1mdiff.sum()} evts")
                indices_cal = detect_peaks(  self.h1np_1mdiff, a=a, b=b  ) # DIFF....
                print(fg.default, end="")

                hname = "h1dif1"
                # NO ZOOM---------------------------------------------------
                self.czoom().cd()                 #print(ROOT.gDirectory.ls())


                # ANALYZE PEAK HERE  when i~= tp
                onenanal = False
                for i in indices_cal:
                    for tp in self.test_peaks:
                        onenanal = True
                        if abs( float(i) - float(tp) )< self.e_window/3: # I will start at the FILE VALUE

                            print(fg.yellow, end="")
                            #zoom_peak(  "h1dif1",  float(i) , self.started , m1_zero, m1_no )
                            print("i.. ANALYZING _PEAK AT  ",i)
                            res = analyze_peak(  "h1dif1",  float(tp) , self.started , m1_zero, m1_no , e_window=self.e_window)
                            print(fg.default,end="")
                            # influxexport  IS IN ANALYZE
                if not onenanal:
                    print("i... demanded peaks not spotted: {self.test_peaks}")

                self.czoom().Modified()
                self.czoom().Update()
                self.c0.Modified()
                self.c0.Update()


                self.c1().cd() # go back to main panel
                #----------------------------end of analyze h1diff1



            if ( now - last2m).total_seconds()>self.HOUR:

                # protection for reset
                if self.h1np.sum() < self.h1np_2m.sum():
                    self.h1np_2m = self.h1np_2m*0
                self.h1np_2mdiff = self.h1np - self.h1np_2m
                self.h1np_2m = self.h1np

                helem = self.h1np_2mdiff
                suma = 0
                for i in range( len(helem) ): # FILL HIST
                    suma+=helem[i]
                    self.h1_dif2.SetBinContent(i, self.h1np_2mdiff[i] )
                self.h1_dif2.SetEntries(suma)

                last2m = now
                #print("                                ******2 ", end = "\n")
                #print("                                 ****** last 2m ", end = "\r")



#===============================================================================









#===============================================================================

if __name__=="__main__":
    Fire(Histogram)
    #h = Histogram()
    #h.draw()
    #h.wait()




#    https://pythonhosted.org/root2matplot/
